describe('Demoblaze - Cart', () => {
  beforeEach(()=> {
    CartPage.getPage();
  });

  it('User places an order', () => {
    const orderData = {
      name: 'John Doe',
      country: 'Argentina',
      city: 'Buenos Aires',
      card: '201616007469588',
      month: '06',
      year: '2020'
    }
    CartPage
      .clickPlaceOrder()
      .fillPlaceOrderForm(orderData)
      .clickPurchase();

    expect(PlaceOrderPage.thankYouAlert()).toBeDisplayed();
  });

  /*
    Skipped due to an error adding products to cart programmatically.
    The second time CartPage.sendAddToCartRequest is called,
    product is not displayed in cart page
   */
  it.skip('User can delete a product', () => {
    CartPage.clickDeleteByIndex(0);
    expect(CartPage.tableRowsProducts()).not.toBeDisplayed();
  });
})
