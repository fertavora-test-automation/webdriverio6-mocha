# WebdriverIO 6 - Mocha

Web Test Framework Example implementing [WebdriverIO 6](https://webdriver.io/) and [Mocha](https://mochajs.org/)

## How to use it
It is asumed that there is a Selenium server running at `localhost:4444`
1. Run `npm i`
2. Run `npm test`

## Details
This framework runs test over the [Demoblaze site](https://demoblaze.com/)

Tests are grouped by site components:

- Home
- Contact
- Product
- Cart

### Framework Modules
The `test` module stores the Mocha specs files.

The `pageObjects` module stores the web app pages structures in Object Literals notation.

### Reporting
As a test results report, the framework calls `wdio-mochawesome-reporter/mergeResults` to generate a consolidated JSON report file.

WIP: Generate HTML report based on JSON file.
